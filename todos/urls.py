from todos.views import TodoListDetailView, TodoListListView
from django.urls import path

urlpatterns = [
    path("", TodoListListView.as_view(), name="list_todos"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="show_todolist"),
]
